# Testové zadání

1) Třídní a funkcionální. Liší se především handlováním lifecyclu. (funkcionální - hooky, třídní - lifecycle metody)
2) Zjednodušují práci s komponentami. Ne, potřeba nejsou.
3) Interface podporuje dědičnost, type nikoliv
4) K omezení přístupu k položkám
5) Ano, slice dělá shallow copy.
6) Forma abstrakce při typování
7) == porovnává evaluovanou hodnotu, === porovnává hodnotu a typ
8) destrukturovaný objekt allProps, kdeprop1 a prop2 jsou dostupné proměnné v aktuálním kontextu.
9) func1 nemá přístup k "this"
10) React.PureComponent dělá pouze shallow comparison
11) Jeho použití ultimátně neguje použití typescriptu.
12) Unknown neumožňuje nemožné operace.
13) Ke zkrácení zápisu logiky skze if-else bloky. || je standardní or, ?? je or, ale na levé straně nekontroluje truthie/falsie, nýbrž jestli proměnná existuje.
14) Volá konstruktor rodiče
15) Slouží jako abstrakce potomků dané komponenty, je dobrá pro práci s potomky
16) pole "res", jež bude obsahovat hodnoty polí arr1 a arr2
17) Ne, dělá pouze shallow copy (A u otázky chybí otazník)
18) Odkaz na daný HTML element
19) typ složený z více jiných typů
20) undefined
21) Pro unikátní identifikaci dané komponenty
22) Metody ne, funkčnost víceméně ano skrze hooky
23) Komponenta nesmí nikdy vracet více prvků na top úrovni.
24) Zobrazení elementu do nodu mimo rodiče
25) Ta druhá bude fungovat, první nemá přístup k this. Muselo by se k té první nabindovat this
26) Kromě poděděných propů z ICoreInterface vlastní IFancyComponent jestě:
    1)  string nebo undefined
    2)  string
    3)  funkci, která bere jeden bool parametr a nic nevrací
    4)  objekt, jehož klíče jsou stringy a hodnoty čísla
    5)  konkrétní slovo z těch vyjmenovaných
    6)  libovolná část objektu, ve kterém jsou možné klíče vyjmenované stringy a hodnoty jsou čísla
    7)  Objekt dle IAnotherInterface, bez propu prop1
    8)  pole stringů (nebo prázdné pole)
    9)  objekt obsahující propy z typu ISomeProps i IAnotherProps