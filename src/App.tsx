import React, { useState } from 'react';
import './App.css';
import Modal, { ModalProps } from './Components/Modal';

function App() {
  const v1: ModalProps = {
    header: {
      buttons: [
        { label: "btn1" },
        { label: "btn2" },
        { label: "btn3" }
      ],
    },
    footer: {
      buttons: [
        { label: "btn4" },
        { label: "close", action: () => updateModalProps(undefined) }
      ]
    }
  }

  const v2: ModalProps = {
    header: {
      buttons: [
        { label: "btnA" },
        { label: "btnB" },
        { label: "btnC" }
      ],
      title: 'This is a different title'
    },
    footer: {
      buttons: [
        { label: "close", action: () => updateModalProps(undefined) },
      ],
    }
  }  

    const v3: ModalProps = {
    header: {
      buttons: [],
      title: ''
    },
    footer: {
      buttons: [
        { label: "close", action: () => updateModalProps(undefined) },
      ],
    }
    }
  
    const v4: ModalProps = {
    header: {
      buttons: [
        { label: "close", action: () => updateModalProps(undefined) }
      ],
    },
    footer: {
      buttons: [],
    }
  }
  const [modalProps, updateModalProps] = useState<ModalProps | undefined>()

  return (
    <div className="App">
      <button onClick={() => updateModalProps(v1)}>v1</button>
      <button onClick={() => updateModalProps(v2)}>v2</button>
      <button onClick={() => updateModalProps(v3)}>v3</button>
      <button onClick={() => updateModalProps(v4)}>v4</button>
      {modalProps ? <Modal {...modalProps} /> : null}
    </div>
  );
}

export default App;
