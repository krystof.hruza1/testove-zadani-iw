import React from 'react'

export interface ButtonProps {
  label: string
  action?: () => void
}

const Button = (props: ButtonProps) => {
  return (
    <button onClick={props.action}>
      {props.label}
    </button >
  )
}

export default Button