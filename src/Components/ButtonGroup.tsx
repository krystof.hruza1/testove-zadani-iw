import React from 'react'
import Button, { ButtonProps } from './Button'

export interface ButtonGroupProps {
  buttons: ButtonProps[]
}

const ButtonGroup = (props: ButtonGroupProps) => {
  return (
    <div>
      {props.buttons.map(btn => <Button {...btn} />)}
    </div >
  )
}

export default ButtonGroup