import React from 'react'
import AdditionalDescription from './AdditionalDescription'
import { ButtonProps } from './Button'
import ButtonGroup from './ButtonGroup'

export interface FooterProps {
  buttons: ButtonProps[]
}

const Footer: React.FC<FooterProps> = (props) => (
  <div>
    <AdditionalDescription />
    <ButtonGroup buttons={props.buttons} />
  </div >
)

export default Footer