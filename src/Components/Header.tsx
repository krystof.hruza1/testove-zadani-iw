import React from 'react'
import { ButtonProps } from './Button'
import ButtonGroup from './ButtonGroup'
import Icon from './Icon'

export interface HeaderProps {
  title?: string
  buttons: ButtonProps[]
}

const Header: React.FC<HeaderProps> = (props) => (
  <div>
    <Icon />
    <span>{props.title ? props.title : 'Default title'}</span>
    <ButtonGroup buttons={props.buttons} />
  </div >
)

export default Header