import React from 'react'
import Content from './Content'
import Footer, { FooterProps } from './Footer'
import Header, { HeaderProps } from './Header'

export interface ModalProps {
  header: HeaderProps
  footer: FooterProps
}

const Modal = (props: ModalProps) => {

  return (
    <div className="modal" style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
      <Header {...props.header} />
      <Content />
      <Footer {...props.footer} />
    </div >
  )
}

export default Modal